
# custom path
export PATH="$HOME/local/bin:$HOME/bin:$PATH"

# show current git branch
export PS1="\[\033[92m\]\u@\h \[\033[94m\]\w\[\033[33m\]\
\$(:(){ git branch 2>/dev/null | grep ^* | sed -e 's/^* \(.*\)/ [\1]/'; };:)\
\n\[\033[94m\]$ \[\033[0m\]"

if ! ll 2>/dev/null; then
  alias ll="ls -al --color --group-directories-first"
fi

